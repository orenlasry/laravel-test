@extends('layouts.app')

@section('title','Create Interview')

@section('content')
    <body>
        <h1>create Interview</h1>
        <form method = "post" action = "{{action('IntreviewController@store')}}">
            @csrf
            <div class="form-group" >
                <label for = "date">Interview date</label>
                <input type = "date" class="form-control" name = "date">
            </div>
            <div class="form-group" >
                <label for = "name">Interview summary</label>
                <input type = "text" class="form-control" name = "summary">
            </div>

            <div class="form-group">
                <label for="candidate_id" class="col-md-4 control-label text-md-right">Candidate</label>
                <div class="col-md-6">
                    <select class="form-control" name="candidate_id">
                        <option value="" disabled selected hidden>Choose Candidate</option>
                        @foreach($candidates as $candidate)
                        <option value="{{ $candidate->id }}">{{ $candidate->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="user_id" class="col-md-4 control-label text-md-right">Interviewer</label>
                    <div class="col-md-6">
                        <select class="form-control" name="user_id">
                            <option value="" disabled selected hidden>{{Illuminate\Support\Facades\Auth::user()->name}}</option>

                            @foreach($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <input type = "submit" class="form-control" name = "submit" value = "Create Interview">
                    </div>
            @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
             @endforeach
            </ul>
            </div>
        @endif
        </form>
    </body>
@endsection
