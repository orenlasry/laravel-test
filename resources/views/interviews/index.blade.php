@extends('layouts.app')

@section('title','Interviews')

@section('content')

@if(Session::has('notallowed'))
<div class='alert alert-danger'>
    {{Session::get('notallowed')}}
</div>

@endif

         @if (Session::has('message'))
            <div class="alert alert-success">{{Session::get('message')}} </div>
        @endif
    <div><a class="badge badge-primary text-wrap" href="{{route('interviews.create')}}">Add new Interview</a></div>

    <h1> List of Interviews</h1>
    <table class="table table-dark">
            <tr>
                <th>id</th><th>Date</th><th>Summary</th><th>Candidates</th><th>Interviewer</th><th>Created</th><th>Updated</th>
            </tr>
        <!-- the table data -->
            @foreach($interviews as $interview)
            <tr>
            <td> {{$interview->id}}</td>
            <td> {{$interview->date}}</td>
            <td> {{$interview->summary}}</td>
            <td> {{$interview->candidateInter->name}}
            <td> {{$interview->userInter->name}}
            <td> {{$interview->created_at}}</td>
            <td> {{$interview->updated_at}}</td>

        </tr>
            @endforeach
    </table>
@endsection
