<?php

use Illuminate\Database\Seeder;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            'date' => date('Y-m-d H:i'),
            'summary' => Str::random(10),
            'created_at' => now(),
           'updated_at' => now(),
           ]);
    }
}
